<div align="center">
  <img src="https://habrastorage.org/webt/zm/nm/pr/zmnmprsvbuxifiuo2dcdb6z1vle.png" alt="" width="750" /><br />
</div>

### THIS REPOSITORY WAS MIGRATED TO GITHUB WITH A LOT OF AWESOME CHANGES: [tarampampam/laravel-roadrunner-in-docker](https://github.com/tarampampam/laravel-roadrunner-in-docker)

---

This repository made for [this article](https://habr.com/ru/post/461687/) <sup>ru</sup> and shows how to integrate Docker and RoadRunner into your Laravel application.

Steps:

1. ["Dockerize" application](https://gitlab.com/tarampampam/laravel-in-docker-with-rr/merge_requests/1)
1. [Makefile and tests added](https://gitlab.com/tarampampam/laravel-in-docker-with-rr/merge_requests/2)
1. [CI integrated](https://gitlab.com/tarampampam/laravel-in-docker-with-rr/merge_requests/3)

Have a nice day! =)
